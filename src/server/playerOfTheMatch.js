const fs = require('fs');
const project = require('./csvtojson.js')  
const matches = project.matches;
const deliveries = project.deliveries;

const result = matches.reduce(function(PoM, current){

    if(!PoM[current["season"]])
    {
        PoM[current["season"]] = {};
        
        PoM[current["season"]][current["player_of_match"]] = 1;
    }
    else
    {
        if(PoM[current["season"]][current["player_of_match"]])
        {
            PoM[current["season"]][current["player_of_match"]] += 1;
        }
        else
        {
            PoM[current["season"]][current["player_of_match"]] = 1;
        }
    }
    return PoM;
},{});

const MoM = Object.entries(result).reduce(function(MoMaccumulate,current){
    let mostMoM = Object.entries(current[1]).sort(function(value1,value2){
        return value2[1] - value1[1];
        
    }).slice(0,1);
    
    MoMaccumulate[current[0]] = Object.fromEntries(mostMoM);
    return MoMaccumulate;
},{})

console.log(MoM);

const playerOfTheMatch = JSON.stringify(MoM);
fs.writeFile('./src/public/output/PlayerOfTheMatch.json',playerOfTheMatch, (err) => {
    if(err)
    {
        throw err;
    }
    else
    {
        console.log("Successfully written to output");
    }
});