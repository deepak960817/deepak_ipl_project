const fs = require('fs');
const project = require('./csvtojson.js')  
const matches = project.matches;
const deliveries = project.deliveries;

const matchSeason = Object.fromEntries(matches.map((value)=>{
    let entry = [];
    entry.push(value["id"]);
    entry.push(value["season"]);
    return entry;
}))


let Runs = 0;
let Balls =0
const runsByBatsman = deliveries.reduce(function(runAccumulator, current){
    
    if(Object.keys(matchSeason).includes(current["match_id"]))
    {
        if(!runAccumulator[matchSeason[current["match_id"]]])
        {
            runAccumulator[matchSeason[current["match_id"]]] = {};
           
                Runs = Number(current["batsman_runs"]);
                if(current["wide_runs"] === '0')
                {
                    Balls = 1;
                }
                else
                {
                    Balls =0
                }
                if(Balls != 0)
                {runAccumulator[matchSeason[current["match_id"]]][current["batsman"]] = (Runs/Balls)*100;}
        }
        else
        {
            if(!runAccumulator[matchSeason[current["match_id"]]][current["batsman"]])
            {
                Runs = Number(current["batsman_runs"]);
                if(current["wide_runs"] === '0')
                {
                    Balls = 1;
                }
                else
                {
                    Balls =0
                }
                if(Balls != 0)
                {runAccumulator[matchSeason[current["match_id"]]][current["batsman"]] = (Runs/Balls)*100;}
            }
            else
            {
                Runs += Number(current["batsman_runs"])
                if(current["wide_runs"] === '0')
                {
                    Balls += 1;
                }
                else
                {
                    Balls += 0;
                }
                if(Balls != 0)
                {runAccumulator[matchSeason[current["match_id"]]][current["batsman"]] = (Runs/Balls)*100;}
            }
        }
    }
    //console.log(runAccumulator[matchSeason[current["match_id"]]]);
    return runAccumulator;
},{});

console.log(runsByBatsman);

const strikeRate = JSON.stringify(runsByBatsman);
fs.writeFile('./src/public/output/StrikeRate.json',strikeRate, (err) => {
    if(err)
    {
        throw err;
    }
    else
    {
        console.log("Successfully written to output");
    }
});