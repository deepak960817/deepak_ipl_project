const fs = require('fs');
const project = require('./csvtojson.js')  
const matches = project.matches;
const deliveries = project.deliveries;

const result = matches.reduce(function(teamWon, current){
    if(teamWon[current["toss_winner"]])
    {
        if(current["toss_winner"] == current["winner"])
        {
            teamWon[current["toss_winner"]] +=1;
        }
        else
        {
            teamWon[current["toss_winner"]] +=0;
        }
    }
    else
    {
        if(current["toss_winner"] == current["winner"])
        {
            teamWon[current["toss_winner"]] =1;
        }
        else
        {
            teamWon[current["toss_winner"]] =0;
        }
    }
    return teamWon;
},{});

console.log(result);

const wonTossWonMatch = JSON.stringify(result);
fs.writeFile('./src/public/output/wonTossWonMatch.json',wonTossWonMatch, (err) => {
    if(err)
    {
        throw err;
    }
    else
    {
        console.log("Successfully written to output");
    }
});