const fs = require('fs');
const project = require('./csvtojson.js')  
const matches = project.matches;
const deliveries = project.deliveries;

const matchID = matches.filter(function(match)
{
    if(match["season"] == "2015")
    {
        return match["id"];
    }
}).map(function(ID){ return ID["id"]});


function season16stats(delivery)
{
    if(matchID.includes(delivery["match_id"]))
    {
        delivery["total_runs"] = Number(delivery["total_runs"]);
        return delivery;
    }
}
const season2015Deliveries = deliveries.filter(season16stats);


function runsConceded(runsGiven, currentValue)
{
    if(runsGiven[currentValue["bowler"]])
    {
        runsGiven[currentValue["bowler"]] += currentValue["total_runs"];  
    }
    else
    {
        runsGiven[currentValue["bowler"]] = currentValue["total_runs"];
    }
    return runsGiven;
}

 const runsConcededByBowlers = season2015Deliveries.reduce(runsConceded, {});
 //console.log(runsConcededByBowlers);
 //console.log(Object.entries(runsConcededByBowlers));


function deliveriesBowled(ballsBowled, currentValue)
{
    if(ballsBowled[currentValue["bowler"]])
    {
        if(currentValue["wide_runs"] == '0' && currentValue["noball_runs"] == '0')
        {
            fairDelivery = 1;
        }
        else
        {
            fairDelivery = 0;
        }
        ballsBowled[currentValue["bowler"]] += fairDelivery 
    }
    else
    {
        ballsBowled[currentValue["bowler"]] =  {};
        if(currentValue["wide_runs"] == '0' && currentValue["noball_runs"] == '0')
        {
            fairDelivery = 1;
        }
        else
        {
            fairDelivery = 0;
        }
        ballsBowled[currentValue["bowler"]] = fairDelivery;
    }
    return ballsBowled;
}

const ballsBowledByBowlers = season2015Deliveries.reduce(deliveriesBowled, {});
//console.log(ballsBowledByBowlers);

function calculateOvers(entries)
{
    let deliveries = entries[1];
    let noOfOvers = (deliveries/6).toString();
    let noOfballs = (deliveries%6).toString();
    Number(noOfOvers.concat('.', noOfballs));
    entries[1] = noOfOvers;
    return entries;
}

const oversBowledByBowlers = Object.entries(ballsBowledByBowlers).map(calculateOvers); 
//console.log(oversBowledByBowlers);

let bowlerNumber = 0;
function calculateEconomy(economy, currentValue)
{
    //console.log(currentValue);
    if(currentValue[0] == oversBowledByBowlers[bowlerNumber][0])
    {
        economy[currentValue[0]] = currentValue[1]/oversBowledByBowlers[bowlerNumber][1];
        bowlerNumber++;
    }
    return economy;
}

const economyBowlersList = Object.entries(runsConcededByBowlers).reduce(calculateEconomy,{});
//console.log(economyBowlersList);

let result = Object.entries(economyBowlersList).sort(function(value1, value2)
{
    if(value1[1]<value2[1])
    {
        return -1
    }
    else if(value1[1]>value2[1])
    {
        return 1;
    }
    else
    {
        return 0;
    }
}).slice(0,10).reduce(function(economy,current)
{
    economy[current[0]] = current[1];
    return economy;
},{});

console.log(result);

const top10economicBowlers = JSON.stringify(result);
fs.writeFile('./src/public/output/top10economicBowlers',top10economicBowlers, (err) => {
    if(err)
    {
        throw err;
    }
    else
    {
        console.log("Successfully written to output");
    }
});