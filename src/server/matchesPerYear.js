const fs = require('fs');
const project = require('./csvtojson.js')  
const matches = project.matches;
const deliveries = project.deliveries;

matches.sort((year1, year2) => {

    return year1.season - year2.season;
});

let matchCounts = 0;
function numberOfMatches(totalMatchesCount, currentValue)
{
        if(totalMatchesCount[currentValue["season"]])
        {
            matchCounts++;
            totalMatchesCount[currentValue["season"]] = matchCounts;            
        }
        else
        {
            matchCounts=1;
            totalMatchesCount[currentValue["season"]] = matchCounts;
        }

        return totalMatchesCount;
}

const yearWiseMatches =  matches.reduce(numberOfMatches, {});

const perYearMatches = JSON.stringify(yearWiseMatches);
fs.writeFile('./src/public/output/matchesPerYear',perYearMatches, (err) => {
    if(err)
    {
        throw err;
    }
    else
    {
        console.log("Successfully written to output");
    }
});