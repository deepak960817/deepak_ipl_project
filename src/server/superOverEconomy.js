const fs = require('fs');
const project = require('./csvtojson.js')  
const matches = project.matches;
const deliveries = project.deliveries;

const runsGivenbyBowler = deliveries.reduce(function(accumulateRuns, current){

    if(current["is_super_over"] === '1')
    {
        if(accumulateRuns[current["bowler"]])
        {
            accumulateRuns[current["bowler"]] += Number(current["total_runs"]);
        }
        else
        {
            accumulateRuns[current["bowler"]] = Number(current["total_runs"]);
        }
    }
    return accumulateRuns;
},{})

const ballsBowled = deliveries.reduce(function(allDeliveries, current){

    if(current["is_super_over"] === '1')
    {
        if(allDeliveries[current["bowler"]])
        {
            if(current["wide_runs"] == '0' && current["noball_runs"] == '0')
            {
                allDeliveries[current["bowler"]] += 1;
            }
            else
            {
                allDeliveries[current["bowler"]] += 0;
            }
        }
        else
        {
            if(current["wide_runs"] == '0' && current["noball_runs"] == '0')
            {
                allDeliveries[current["bowler"]] = 1;
            }
            else
            {
                allDeliveries[current["bowler"]] = 0;
            }
        
        }
    }
    return allDeliveries;
},{})

const calculateOvers = Object.entries(ballsBowled).reduce(function(overAccumulate, current){

    let noOfBalls = current[1];
    let over = (noOfBalls/6);
    overAccumulate[current[0]] = over;
    return overAccumulate;

},{})

let count =0;
const calculateEconomy = Object.entries(runsGivenbyBowler).reduce(function(economy, current){

    economy[current[0]] = current[1]/Object.entries(calculateOvers)[count++][1];
    return economy;
},[])

const topEconomicBowlerInSuperOver = Object.entries(calculateEconomy).sort(function(value1, value2){
    
    return value1[1]- value2[1];
}).slice(0,1);

let EconomicBowlerInSuperOver = Object.fromEntries(topEconomicBowlerInSuperOver);
console.log(EconomicBowlerInSuperOver);


const superOverEconomicBowler = JSON.stringify(EconomicBowlerInSuperOver);
fs.writeFile('./src/public/output/SUperOverEconomicBowler.json',superOverEconomicBowler, (err) => {
    if(err)
    {
        throw err;
    }
    else
    {
        console.log("Successfully written to output");
    }
});