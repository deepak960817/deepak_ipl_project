const fs = require('fs');
const project = require('./csvtojson.js')  
const matches = project.matches;
const deliveries = project.deliveries;

const sortedByYearAndWinner = matches.sort((parameter1, parameter2) => {

    if(parameter1.season == parameter2.season)
    {
        if(parameter1.winner < parameter2.winner)
        {
            return -1;
        }
        else if(parameter1.winner > parameter2.winner)
        {
            return 1;
        }
        return 0;
    }
    else
    {
        return parameter1.season - parameter2.season;
    }
});

//console.log(sortedByYearAndWinner);

let winCounts = 0;
function numberOfMatchesWon(winners, currentValue)
{   
        if(!winners[currentValue["season"]])
        {
            winners[currentValue["season"]] = {};
            winCounts = 1;
            winners[currentValue["season"]][currentValue["winner"]] = winCounts;
        }
        else
        {
            if(winners[currentValue["season"]][currentValue["winner"]])
            {
                winCounts++;
                winners[currentValue["season"]][currentValue["winner"]] = winCounts;
            }           
            else
            {
                winCounts=1;
                winners[currentValue["season"]][currentValue["winner"]] = winCounts;
            }
        }

        return winners;
}

const result = sortedByYearAndWinner.reduce(numberOfMatchesWon, {});

const matchesWonPerTeamPerYear = JSON.stringify(result);
fs.writeFile('./src/public/output/matchesWonPerTeamPerYear',matchesWonPerTeamPerYear, (err) => {
    if(err)
    {
        throw err;
    }
    else
    {
        console.log("Successfully written to output");
    }
});