const fs = require('fs');
const project = require('./csvtojson.js')  
const matches = project.matches;
const deliveries = project.deliveries;;

const matchID = matches.filter(function(match)
{
    if(match["season"] == "2016")
    {
        return match["id"];
    }
}).map(function(ID){ return ID["id"]});

//console.log(matchID);

function season16stats(delivery)
{
    if(matchID.includes(delivery["match_id"]))
    {
        delivery["extra_runs"] = parseInt(delivery["extra_runs"]);
        return delivery;
    }
}

const season2016Deliveries = deliveries.filter(season16stats);
//console.log(season2016Deliveries);


function extras(extrasPerTeamin2016, currentValue)
{
    if(!extrasPerTeamin2016["Extra Per Team in 2016"])
    {
        extrasPerTeamin2016["Extra Per Team in 2016"] = {};
    }
    if(extrasPerTeamin2016["Extra Per Team in 2016"][currentValue["bowling_team"]])
    {
        extrasPerTeamin2016["Extra Per Team in 2016"][currentValue["bowling_team"]] += currentValue["extra_runs"];            
    }
    else
    {
        extrasPerTeamin2016["Extra Per Team in 2016"][currentValue["bowling_team"]] = currentValue["extra_runs"];
    }

    return extrasPerTeamin2016;
}

const result = season2016Deliveries.reduce(extras, {});
//console.log(result);

const extrasPerTeam = JSON.stringify(result);
fs.writeFile('./src/public/output/extrasPerTeam.json',extrasPerTeam, (err) => {
    if(err)
    {
        throw err;
    }
    else
    {
        console.log("Successfully written to output");
    }
});