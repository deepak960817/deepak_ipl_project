const fs = require('fs');
const project = require('./csvtojson.js')  
const matches = project.matches;
const deliveries = project.deliveries;

const dismissed = deliveries.reduce(function(mostDismissal, current){

    let bowlerEntry = {};
    if(current["player_dismissed"] !== "")
    {
        if(mostDismissal[current["player_dismissed"]])
        {
            if(mostDismissal[current["player_dismissed"]][current["bowler"]])
            {
                mostDismissal[current["player_dismissed"]][current["bowler"]] +=1;
            }
            else
            {
                let key = current["bowler"]
                bowlerEntry[key] = 1;
                Object.assign(mostDismissal[current["player_dismissed"]],(bowlerEntry));
            }
        }
        else
        {
            mostDismissal[current["player_dismissed"]] = {};
            let key = current["bowler"]
            bowlerEntry[key] = 1;
            bowlerEntry[current["bowler"]] = 1;
            Object.assign(mostDismissal[current["player_dismissed"]],(bowlerEntry));
        }
    }
    return mostDismissal;
},{})

const mostDismissed = Object.entries(dismissed).reduce(function(accumulate, currVal){

    let sortedVal = Object.entries(currVal[1]).sort(function(value1, value2){
        return value2[1] - value1[1];
    })
    //console.log(sortedVal)
    accumulate[currVal[0]]  = Object.fromEntries(sortedVal.slice(0,1));
    return accumulate;
},{})

console.log(mostDismissed);

const playerDIsmissed = JSON.stringify(mostDismissed);
fs.writeFile('./src/public/output/playerDismissed.json',playerDIsmissed, (err) => {
    if(err)
    {
        throw err;
    }
    else
    {
        console.log("Successfully written to output");
    }
});