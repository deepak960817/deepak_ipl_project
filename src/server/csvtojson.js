let csvToJson = require('convert-csv-to-json');

const matchesInputFile = './src/data/matches.csv';
const deliveriesInputFile = './src/data/deliveries.csv';
const matches = csvToJson.fieldDelimiter(',').getJsonFromCsv(matchesInputFile);
const deliveries = csvToJson.fieldDelimiter(',').getJsonFromCsv(deliveriesInputFile);

module.exports = {matches: matches, deliveries : deliveries};
