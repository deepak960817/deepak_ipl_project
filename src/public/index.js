// Create the chart
fetch('./output/matchesPerYear.json').then((res) => res.json()).then((data1) => {
    chartMatchPerYear(data1);
})

function chartMatchPerYear(data1) {
Highcharts.chart('chart1', {
    chart: {
        type: 'column'
    },
    title: {
        //align: 'left',
        text: 'IPL Matches Played Per Year'
    },
    accessibility: {
        announceNewData: {
            enabled: true
        }
    },
    xAxis: {
        type: 'category',
        title: {
            text: "Seasons"
        }
    },
    yAxis: {
        title: {
            text: 'Matches Played'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.1f}'
            },
            pointStart: 2008
            
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> of total<br/>'
    },

    series: [
        {
            name: "Years",
            colorByPoint: true,
            data: Object.values(data1)
        }
    ]
})
}